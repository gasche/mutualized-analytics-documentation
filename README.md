# Simple mutualized web analytics

The goal of this work is to provide a solution for simple web
analytics (daily, monthly, yearly visit count) to be used by
websites such as personal blogs.

## Design

Counting visits requires some server-side logic, so there has to
be a dynamic server in the loop. We designed the solution to be
mutualized: several static websites can use a single dynamic
server to collect their statistics. We respected the following
additional criterions:

- Built entirely on top of free software

- Self-hosted: the host of the dynamic website is in control of
  the analytics data, which never went through a third-party.

- Easy to deploy and maintain: it is built from widely available
  components that can be installed and updated through your
  distribution package manager.

- Privacy-respecting: access little information from visitors¹,
  and do not keep non-anonymized data for more than a month.

- Simple: we provide only the necessary features, using only the
  necessary software. We hope that this helps make the solution
  more efficient (low resource usage) and more secure
  (small attack surface), but we focus on reusing existing
  components and maintainability; home-grown solutions could
  improve efficiency and security, at the cost of additional
  maintenance.

¹: we store the IP, to distinguish unique from repeat visits,
and the UserAgent, to recognize friendly bots.

## Implementation

The analytics server must run a web server such as Apache, Nginx
or Lighttpd. For each website client, it provides a "visit URL"
for each monitored page of the client website, and an "analytics
URL" at which the resulting statistics can be visualized.

Accessing any "visit URL" returns no data, but the visit is
stored in the access logs of the web server. The 'webalizer'
program ( http://www.webalizer.org/ ) is run daily by a cron job
on the server machine, and consumes the access logs of the
"visit URLs" to populate the "analytics URL" with static HTML
webpages.

The analytics admin thus needs to set up three components:

- A set of visit URLs and an access log file for each for each
  analytics client; this is done in the webserver configuration

- A cron job running 'webalizer' on the access log file of each
  client, populating an analytics directory (separate for
  each client)

- An analytics URL to access the webalizer result for each
  client. This is done in the webserver configuration; it may
  involve setting access restrictions if you don't wish the
  analytics result to be publicly accessible.

Each analytics client needs to add a bit of HTML or Javascript
code to their webpages to ensure that each visit to their
website sends a request to the corresponding visit URL on the
analytics server.

Note: the webserver logs typically keep the user IP, the access
time, and the User Agent string. All three potentially
constitute de-anonymizing information and should only be kept if
necessary. The reason why you need to keep them for analytics is
to identity repeat visitors to be able to count "unique
visits". The 'webalizer' program aggregates statistics per
month, so we only need to identify repeated visit during one
month spans, and we can remove all logged IP adresses at the end
of each month.


### Security threats

Running a webserver is a source of security
vulnerabilities. This solution is designed to use no more
capabilities than a webserver already provides for most common
needs, so as not to increase the attak surface.

The 'webalizer' program is a C program that manipulates
untrusted data. One should assume that it will be source of
security vulnerabilities; on the other hand, it has been around
for years and seems relatively widely used. A good design would
be to sandbox its execution, although we have not worked on this
yet. There is a lack of good security sandboxes easily available
on most distributions -- mbox
( https://pdos.csail.mit.edu/archive/mbox/ ) seems promising but
does not have a Debian or Fedora package as of today.

Even when 'webalizer' is sandboxed, malicious inputs could make
it write garbage or malicious data to the HTML webpages of the
analytics result. This could in theory result in
cross-site-scripting attacks. We could postprocess the webalizer
output with HTML sanitization tools, but this is left to future
work.

Malicious users can falsify your statistics by generating
spurious requests to visit URLs. There is not much we can do
against this -- after all, they can also falsify the statistics
by accessing your website without the intent to read it. If we
were willing to add a server-side component to the client
website, we could have the client website and analytics server
share a secret and generate a challenge to identify themselves,
but if the client website has a dynamic component why offload
analytics in the first place?


## Setup

We rely on the packages `webalizer`, `cron`, `logrotate`, and
a webserver of your choice (`lighttpd`, `apache2`, `nginx`,
etc.). The configuration depends on the webserver used.

For each analytics client `toto`, decide on a URL scheme for the
visit URLs and analytics URL, and a directory on the server in
which to store the analytics results. Hereafter we will use
TOTOURL/visit as the root of the visit URLs, TOTOURL/view for
the analytics URL, and TOTODIR for the filesystem path to the
analytics results.

More precisely, for a client website URL of the form

    <protocol>://<hostname>/<path>

the corresponding visit URL will be of the form

    TOTOURL/visit/<hostname>/<path>

## client setup

It suffices to have on each of your webpages an element that
will cause a request to a sub-url of TOTOURL/visit.

I personally prefer using a POST request for this (because they
are precisely intended for non-indempotent action), which I do
with the following bit of Javascript code:

    /* a privacy-respecting analytics server,
       see TOTOURL/about */
    var analytics_server = TOTOURL;
    var http = new XMLHttpRequest();
    var hostname = encodeURIComponent(window.location.hostname);
    var url_suffix = window.location.pathname + window.location.search;
    var url = analytics_server + "/visit/" + hostname  + "/" + url_suffix;
    http.open("POST", url, true);
    http.setRequestHeader("Content-length", 0);
    http.send();

You could also use a statically-generated HTML snippet for this,
for example

    <img src="TOTOURL/visit/HOSTNAME/PATH"
         alt="privacy-friendly analytics request, see TOTOURL/about" />

but that requires coordination with the author/generator of the
web pages -- each website page needs a different PATH value.

## general server setup

Create the analytics result directory:

    mkdir -p TOTODIR
    mkdir TOTODIR/output

Create an empty file TOTODIR/empty, which will be served when
accessing TOTOURL/visit (this is nicer than returning a 404
error to visitors of the client website):

    touch TOTODIR/empty

### webalizer setup

Write the following configuration in TOTODIR/webalizer.conf:

    LogFile TOTODIR/visit.log
    OutputDir TOTODIR/output
    HistoryName TOTODIR/webalizer.hist
    Incremental Yes
    IncrementalName TOTODIR/webalizer.state

The `Incremental` parameter makes webalizer keep some state from one
run to the other, which means that we do not need to keep log files
between two runs. Non-anonymzed data may remain in webalizer's cache
in the `webalizer.state` file, which is cleared every month (see the
`clear_month()` function in `webalizer.c`).

You may add arbitrary other configuration parameters to tune the
webalizer output, see /etc/webalizer.conf or
ftp://ftp.mrunix.net/pub/webalizer/sample.conf

### logrotate and cron setup

Add an empty file TOTODIR/visit.log, giving your webserver the rights
to access it:

    touch TOTODIR/visit.log
    sudo chown www-data TOTODIR/visit.log

Add a file TOTODIR/logrotate.conf with the following content --
`rotate 0` ensures that non-anonymized log files are not kept longer
than a day. (Non-anonymized data may remain in webalizer's cache for
up to a month, however.)

    TOTODIR/visit.log {
        daily
        rotate 0
    }

and a file TOTODIR/cronjob with the following content:

    #!/bin/sh
    webalizer -c TOTODIR/webalizer.conf
    logrotate --state TOTODIR/logrotate.state TOTODIR/logrotate.conf

and create a symbolic link to it from `/etc/cron.daily`:

    sudo ln -s TOTODIR/cronjob /etc/cron.daily/toto-analytics

### webserver setup: Lighttpd

Let TOTOURLPATH be the PATH to TOTOURL from your webserver point
of view. For example, if TOTOURL is zygomar.eu/analytics/toto,
and the webserver hosts zygomar.eu, the path will be
/analytics/toto. If you use a more sophisticated URL routing
setup (subdomains etc.) we trust you to modify the configuration
below accordingly.

Create a file TOTODIR/lighttpd.conf with the following content:

    $HTTP["url"] =~ "^TOTOURLPATH/visit" {
        accesslog.filename   = "| cat > TOTODIR/visit.log"
        alias.url += ( "" => "TOTODIR/empty" )
    }

    $HTTP["url"] =~ "^TOTOURLPATH/view" {
        alias.url += ( "TOTOURLPATH/view" => "TOTODIR/output" )
    }

and create a symbolic link to it from `/etc/lighttpd`:

    sudo ln -s TOTODIR/lighttpd.conf \
      /etc/lighttpd/conf-enabled/80-toto-analytics.conf

Note: TOTOURLPATH should start with a '/'. While replaying these
instructions myself, I repeatedly forgot to include the leading '/'.

Note: we use the piped logs syntax "| ...". This avoids having to
restart lighttpd when rotating the logs.

If you wish to make the analytics access more private, you can
also add the following:

    server.modules += ( "mod_auth" )

    $HTTP["url"] =~ "^TOTOURL/view" {
        auth.backend = "plain"
        auth.require = ( "" => ( "method" => "basic", "realm" => "analytics results for toto", "require" => "valid-user" ) )
        auth.backend.plain.userfile = "TOTODIR/auth"
    }

This sets up very basic authentification: TOTODIR/auth should be
a text file with lines of the form "foo:bar" where "foo" is
a user login and "bar" is the corresponding
password. (See lighttpd documentation for more advanced
authentification mechanisms.)

You can now enable this analytics setting by running

    sudo lighttpd-enable-mod analytics-toto

to enable the analytics component for "toto" (you then need to
restart the web server), and

    sudo lighttpd-disable-mod analytics-toto

to disable it again.

### Recap

At the end of your server configuration, the following files should be
present in TOTODIR:

    cronjob empty lighttpd.conf logrotate.conf output/ visit.log webalizer.conf

The following files will be added once webalizer starts running:

    webalizer.hist webalizer.state
